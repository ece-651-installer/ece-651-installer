#!/bin/bash

color_profile_settings="
[Allow Colord all Users]
Identity=unix-user:*
Action=org.freedesktop.color-manager.create-device;org.freedesktop.color-manager.create-profile;org.freedesktop.color-manager.delete-device;org.freedesktop.color-manager.delete-profile;org.freedesktop.color-manager.modify-device;org.freedesktop.color-manager.modify-profile
ResultAny=no
ResultInactive=no
ResultActive=yes
"

polkit_file="/etc/polkit-1/localauthority/50-local.d/45-allow-colord.pkla"

sudo echo "$color_profile_settings" | sudo tee -a "$polkit_file"
