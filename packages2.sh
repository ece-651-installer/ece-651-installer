#!/bin/bash


# Install emacs and java.
DEBIAN_FRONTEND=noninteractive sudo apt install emacs unzip openjdk-17-jdk clang-format -y

#git config --global credential.helper store
#We want 7.3.3 in particular.
GRADLE_VERSION=7.3.3

wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip
sudo mkdir -p /opt/gradle
sudo unzip -d /opt/gradle gradle-${GRADLE_VERSION}-bin.zip
rm gradle-${GRADLE_VERSION}-bin.zip
#for this time
export PATH=${PATH}:/opt/gradle/${GRADLE_VERSION}/bin


# Add extra auto complete support to base for gradle commands
sudo DEBIAN_FRONTEND=noninteractive  apt install gradle-completion -y



#adding files to /etc/skel so that they get populated in home directories
dirs=(/home/vcm "/etc/skel")
for i in "${dirs[@]}"
do
  sudo echo "export PATH=\"\${PATH}:/opt/gradle/gradle-${GRADLE_VERSION}/bin\"" | sudo tee -a $i/.bashrc > /dev/null
  sudo echo "XTerm.termName: xterm-256color" | sudo tee -a $i/.Xresources
  sudo echo "xterm*faceName: Deja Sans Mono Book" | sudo tee -a $i/.Xresources
  sudo echo "xterm*faceSize: 18" | sudo tee -a $i/.Xresources

  sudo echo "export EDITOR='emacs -nw'" | sudo tee -a $i/.bashrc > /dev/null
  sudo echo "export VISUAL=emacs" | sudo tee -a $i/.bashrc > /dev/null
done



