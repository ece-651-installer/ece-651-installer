#!/bin/bash

set -x
INVENTORY=~/.rapid_ansible_hosts

echo localhost ansible_connection=local > $INVENTORY

## Do the install here
ansible-playbook -i $INVENTORY ./fastx.yaml | tee /tmp/rapid_image_status.txt
./packages2.sh | tee /tmp/rapid_image_status.txt
./emacs-config.sh | tee /tmp/rapid_image_status.txt
./color_profile.sh | tee /tmp/rapid_image_status.txt
sudo cp ./clang-format /etc/skel/.clang-format
sudo cp ./clang-format /home/vcm/.clang-format
sudo chown -R vcm:vcm /home/vcm

if [[ ${PIPESTATUS[0]} == 0 ]]; then
  echo "Installer Succeeded"
  echo "success" > /tmp/rapid_image_complete
else
  echo "Installer failed"
  echo "fail" > /tmp/rapid_image_complete
fi
